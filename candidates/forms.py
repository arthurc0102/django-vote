from core.forms import BootstrapModelForm

from .models import Candidate


class CandidateModelForm(BootstrapModelForm):
    class Meta:
        model = Candidate
        fields = ['last_name', 'first_name', 'age', 'politics']
