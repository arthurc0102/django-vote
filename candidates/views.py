from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.messages import add_message as m, SUCCESS, ERROR

from .models import Candidate
from .forms import CandidateModelForm


def index(request):
    candidates = Candidate.objects.all()
    return render(request, 'candidates/index.html', {'candidates': candidates})


def show(request, pk):
    candidate = get_object_or_404(Candidate, pk=pk)
    return render(request, 'candidates/show.html', {'candidate': candidate})


def create(request):
    form = CandidateModelForm(request.POST or None)
    if form.is_valid():
        candidate = form.save()
        m(request, SUCCESS, 'Create success')
        return redirect('candidates:show', candidate.id)

    return render(request, 'candidates/create.html', {'form': form})


def edit(request, pk):
    candidate = get_object_or_404(Candidate, pk=pk)
    form = CandidateModelForm(request.POST or None, instance=candidate)
    if form.is_valid():
        form.save()
        m(request, SUCCESS, 'Edit success')
        return redirect('candidates:show', pk)

    return render(request, 'candidates/edit.html', {'form': form})


def delete(request, pk):
    candidate = get_object_or_404(Candidate, pk=pk)
    if request.method == 'POST':
        candidate.delete()
        m(request, SUCCESS, 'Delete success')
        return redirect('candidates:index')

    return render(request, 'candidates/delete.html', {'candidate': candidate})


def vote(request, pk):
    if request.method == 'POST':
        candidate = get_object_or_404(Candidate, pk=pk)
        candidate.vote += 1
        candidate.save()
        m(request, SUCCESS, 'Vote success')
    else:
        m(request, ERROR, 'Vote error')

    return redirect('candidates:index')
