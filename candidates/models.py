from django.db import models


class Candidate(models.Model):
    first_name = models.CharField('名字', max_length=10)
    last_name = models.CharField('姓氏', max_length=10)
    age = models.PositiveSmallIntegerField('年紀')
    politics = models.TextField('政見', max_length=300)
    vote = models.PositiveIntegerField('得票數', default=0)

    def __str__(self):
        return self.full_name()

    def full_name(self):
        return '{} {}'.format(self.last_name, self.first_name)
